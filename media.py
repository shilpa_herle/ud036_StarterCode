class Movie():
    """ Class to hold the information related to movies
    Attributes:
        title: A String to store the title of the movie
        poster_image_url: A String to store the url/path of the poster
            image of movie
        trailer_youtube_url: A String to store the youtube trailer of
            the movie
    """
    def __init__(self, title, poster_image_url, trailer_youtube_url):
        """ Inits Movie class with title, poster_image_url and
            trailer_youtube_url"""
        self.title = title
        self.poster_image_url = poster_image_url
        self.trailer_youtube_url = trailer_youtube_url
