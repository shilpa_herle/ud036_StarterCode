import media
import fresh_tomatoes

# create movie objects by initializing Movie class with appropriate data
toy_story_3 = media.Movie("Toy Story 3", "static/toy_story_3.jpeg",
                          "https://www.youtube.com/watch?v=JcpWXaA2qeg")
tangled = media.Movie("Tangled", "static/tangled.jpg",
                      "https://www.youtube.com/watch?v=2f516ZLyC6U")
cars_2 = media.Movie("Cars 2", "static/cars_2.jpg",
                     "https://www.youtube.com/watch?v=zonotSm4Mdc")
frozen = media.Movie("Frozen", "static/frozen.jpeg",
                     "https://www.youtube.com/watch?v=TbQm5doF_Uc")
inside_out = media.Movie("Inside Out", "static/inside_out.jpg",
                         "https://www.youtube.com/watch?v=yRUAzGQ3nSY")
zootopia = media.Movie("Zootopia", "static/zootopia.jpeg",
                       "https://www.youtube.com/watch?v=jWM0ct-OLsM")
jungle_book = media.Movie("The Jungle Book", "static/jungle_book.jpg",
                          "https://www.youtube.com/watch?v=5mkm22yO-bs")
moana = media.Movie("Moana", "static/moana.jpeg",
                    "https://www.youtube.com/watch?v=LKFuXETZUsI")
planes = media.Movie("Planes", "static/planes.jpeg",
                     "https://www.youtube.com/watch?v=YRjztG65XgI")

# create a list of movies to be passed onto open_movies_page
movies = [toy_story_3, tangled, cars_2, frozen, inside_out, zootopia,
          jungle_book, moana, planes]
fresh_tomatoes.open_movies_page(movies)
