# Movie Trailer Website
The Movie Trailer Website project consists of server-side code to store a list of movies titles, along with its respective box art imagery and movie trailer. The data is served as a web page allowing visitors to watch the trailers.

## Prerequisites
Install Python
Install Idle

## Installing
git clone this project
Open entertainment\_center.py using IDLE and run(F5) the file.

## Output
A browser opens with tile list of movie posters and shows the trailer upon clicking a particular movie

## Customization
The list of movies can be modified or new movies can be added in the entertainment\_center.py by following the code used for creating movie objects.
The poster images for the new movies must be placed inside the 'static' folder
